package main

import (
	"log"
	"github.com/labstack/echo"
	"net/http"
	"proxeus.com/scraper/service"
	"github.com/labstack/echo/middleware"
	"math/big"
)

var proxeusContract = "0xA017ac5faC5941f95010b12570B812C974469c2C"

func main() {
	transactionService, err := new(service.DefaultTransactionService).New()
	if err != nil {
		log.Fatal(err)
	}

	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))
	e.GET("/", func(c echo.Context) error {
		transactions, _ := transactionService.GetAll()
		return c.JSON(http.StatusOK, transactions)
	})

	err = transactionService.Init(proxeusContract, big.NewInt(4974811)) // Proxeus's first transaction in contract appeared at 4974811. No need to start from genesis
	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(e.Start(":1323"))
}