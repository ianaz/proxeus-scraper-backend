CREATE TABLE `transactions` (
  `address` VARCHAR(42) NULL,
  `blocknumber` INT(20) NULL,
  `hash` VARCHAR(80) NULL,
  `topic` VARCHAR(30) NULL,
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`));

ALTER TABLE `transactions`
ADD COLUMN `time` DATETIME(6) NULL AFTER `id`;

ALTER TABLE `transactions`
ADD COLUMN `value` VARCHAR(60) NULL AFTER `date`;
