package typex

import (
	"time"
	"math/big"
)

type Transactions []Transaction
type Transaction struct {
	Id int64 // identifier
	Address string
	Hash string
	Topic string
	BlockNumber uint64
	Time time.Time
	Value *big.Int
}
