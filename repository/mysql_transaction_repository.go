package repository

import (
	"proxeus.com/scraper/typex"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/gommon/log"
	"math/big"
)

type MySQLTransactionRepository struct{}

var db *sql.DB
var tableName = "transactions"


// Don't forget to call defer .Close()
func (r MySQLTransactionRepository) Connect(url string) (TransactionRepository, error) {
	var err error
	db, err = sql.Open("mysql", url +"?parseTime=true")
	if err != nil {
		return nil, err
	}
	return r, nil
}

func (r MySQLTransactionRepository) Close() {
	db.Close()
}

func (r MySQLTransactionRepository) Save(transactions *typex.Transactions) (*typex.Transactions, error) {
	// TODO: transaction db.BeginTx()
	for _, transaction := range *transactions {
		res, err := db.Exec(
			"insert into "+tableName+" (address, blocknumber, hash, topic, value, time) values(?, ?, ?, ?, ?, ?)",
			transaction.Address,
			transaction.BlockNumber,
			transaction.Hash,
			transaction.Topic,
			transaction.Value.String(),
			transaction.Time,
		)
		if err != nil {
			return nil, err
		}
		id, _ := res.LastInsertId()
		transaction.Id = id
	}
	//tx.Commit()
	return transactions, nil
}

func (r MySQLTransactionRepository) GetAll() (typex.Transactions, error) {
	rows, err := db.Query("SELECT id, address, blocknumber, hash, topic, value, time FROM " + tableName + " order by id desc")
	if err != nil {
		return nil, err
	}
	return rowsToTransactions(rows), nil
}

// Transforms sql.Rows to Transactions
func rowsToTransactions(rows *sql.Rows) typex.Transactions {
	transactions := typex.Transactions{}
	for rows.Next() {
		transaction := typex.Transaction{}
		var transactionValue int64
		err := rows.Scan(&transaction.Id, &transaction.Address, &transaction.BlockNumber, &transaction.Hash, &transaction.Topic, &transactionValue, &transaction.Time)
		transaction.Value = big.NewInt(transactionValue)
		if err != nil {
			log.Error("Can't transform row to struct", err)
		}
		transactions = append(transactions, transaction)
	}
	return transactions
}

func (r MySQLTransactionRepository) Count() (uint32, error) {
	rows, err := db.Query("select count(*) as total from " + tableName + "")
	if err != nil {
		return 0, err
	}
	var total int
	rows.Next()
	err = rows.Scan(&total)
	if err != nil {
		return 0, err
	}
	return uint32(total), err
}

func (r MySQLTransactionRepository) GetLast() (typex.Transaction, error) {
	var transaction typex.Transaction
	rows, err := db.Query("select id, address, blocknumber, hash, topic, value, time from " + tableName + " order by id desc limit 1")
	if err != nil {
		return transaction, err
	}
	transactions := rowsToTransactions(rows)
	if len(transactions) > 0 {
		transaction = transactions[0]
	}
	return transaction, err
}
