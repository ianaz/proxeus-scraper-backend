package repository

import "proxeus.com/scraper/typex"

type TransactionRepository interface {
	Connect(url string) (TransactionRepository, error)
	Save(*typex.Transactions) (*typex.Transactions, error)
	GetAll() (typex.Transactions, error)
	Count() (uint32, error)
	GetLast() (typex.Transaction, error)
}