package repository

import (
	"testing"
	"proxeus.com/scraper/typex"
	"time"
	"math/big"
)

func TestSave (t *testing.T) {
	transactionRepository, _ := new(MySQLTransactionRepository).Connect("root:@/scraper")
	transactions := typex.Transactions{
		typex.Transaction{
			BlockNumber: 100,
			Time: time.Now(),
			Value: big.NewInt(1000),
			Address: "0x0001010101010101010101010",
			Topic: "transfer",
			Hash: "0x93393939393939393939",
		},
	}
	_, err := transactionRepository.Save(&transactions)
	if err != nil {
		t.Fatal("Error", err)
	}
}