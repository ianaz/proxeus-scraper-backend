package service

import (
	"testing"
	"math/big"
	"log"
)

func TestEthereumBlockchainConnector_getBlockChunks(t *testing.T) {
	// I expect [10 21 31 41] and [20 30 40 42]
	startBlocks, toBlocks := getBlockChunks(big.NewInt(10), big.NewInt(42), 10)
	log.Println(startBlocks)
	log.Println(toBlocks)
	if len(startBlocks) == 0 {
		t.Error("Array is empty")
	}
	if len(startBlocks) != 4 {
		t.Error("Start blocks are", len(startBlocks), "while they should be 3")
	}
	if len(startBlocks) != len(toBlocks) {
		t.Error("Start and To blocks aren't the same size")
	}
	if startBlocks[1].Cmp(big.NewInt(21)) != 0 {
		t.Errorf("The second chunk should start from 21 but is %d", startBlocks[1])
	}
}