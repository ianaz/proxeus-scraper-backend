package service

import (
	"testing"
	"proxeus.com/scraper/mocks"
	"github.com/golang/mock/gomock"
	"errors"
	"math/big"
)

var contractAddress = "anyContract"

func TestGetTransactionsByContract(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	transactionService := TransactionService{}
	blockchainConnector := mocks.NewMockBlockchainConnector(mockCtrl)

	t.Run("ShouldThrowErrorOnFailingToConnectToNode", func(t *testing.T) {
		blockchainConnector.EXPECT().Connect(gomock.Any()).Return(errors.New("new error"))
		blockchainConnector.EXPECT().GetBlockByNumber(gomock.Any()).Times(0)
		_, err := transactionService.GetTransactionsByContract(blockchainConnector, contractAddress, big.NewInt(0))
		if err == nil {
			t.Error("Error shoulnd't be null")
		}
	})

	t.Run("ShouldNotReturnErrorOnConnectionToEthereumNodeSuccessful", func(t *testing.T) {
		blockchainConnector.EXPECT().Connect(gomock.Any()).Return(nil)
		blockchainConnector.EXPECT().GetBlockByNumber(gomock.Any()).Times(1)
		transactionService.GetTransactionsByContract(blockchainConnector, contractAddress, big.NewInt(0))
	})

}