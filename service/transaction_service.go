package service

import (
	"net/http"
	"math/big"
	"proxeus.com/scraper/typex"
	"proxeus.com/scraper/repository"
	"log"
)

var ethereumNodeAddress = "https://mainnet.infura.io/v3/4876e0df8d31475799c8239ba2538c4c" // "ws://91.121.173.37:8546"
var client = http.Client{}

var transactionRepository repository.TransactionRepository

type TransactionService interface {
	New() (TransactionService, error)
	Init(contractAddress string, startBlock *big.Int) (error)
	GetTransactionsByContract(bcConnector BlockchainConnector, account string, startBlock *big.Int)
	GetAll() (typex.Transactions, error)
}

// Implementation starts here
type DefaultTransactionService struct{}

func (ts DefaultTransactionService) New() (DefaultTransactionService, error) {
	var err error
	transactionRepository, err = new(repository.MySQLTransactionRepository).Connect("root:@/scraper")
	if err != nil {
		return ts, err
	}
	return ts, nil
}

// Initialization. Checks whether it's an empty db and retrieves old data
func (ts DefaultTransactionService) Init(contractAddress string, startBlock *big.Int) (error) {
	lastTransaction, err := transactionRepository.GetLast()
	log.Println("Last transaction:", lastTransaction)
	if err != nil {
		return err
	}
	if (lastTransaction != typex.Transaction{}) { // If found.....
		startBlock = big.NewInt(int64(lastTransaction.BlockNumber)+1)
	}
	log.Println("Retrieving transactions from", startBlock, "to now")
	blockchainConnector := new(EthereumBlockchainConnector)
	transactions := make(chan typex.Transaction)
	go func() {
		for {
			select {
			case transaction, done := <-transactions:
				if !done {
					break
				}
				log.Println("Saving transaction")
				_, err = transactionRepository.Save(&typex.Transactions{
					transaction,
				})
				if err != nil {
					log.Fatal(err)
				}
			}
		}
		log.Println("Transaction channel closed, leaving...")
	}()
	ts.GetTransactionsByContract(blockchainConnector, contractAddress, startBlock, transactions)
	log.Println("Listening to incoming transactions")
	return err
}

func (ts DefaultTransactionService) GetTransactionsByContract(bcConnector BlockchainConnector, account string, startBlock *big.Int, transactions chan<- typex.Transaction) {
	err := bcConnector.Connect(ethereumNodeAddress)
	if err != nil {
		log.Fatal(err)
	}
	lastBlock, err := bcConnector.GetBlockByNumber(nil)
	if err != nil {
		log.Fatal(err)
	}
	bcConnector.GetERC20Transfers(account, startBlock, lastBlock.Number(), transactions)
}

func (ts DefaultTransactionService) GetAll() (typex.Transactions, error) {
	return transactionRepository.GetAll()
}
