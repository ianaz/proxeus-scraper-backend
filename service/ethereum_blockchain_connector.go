package service

import (
	"math/big"
	"context"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/core/types"
	"log"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum"
	"net/http"
	"bytes"
	"io/ioutil"
	"proxeus.com/scraper/typex"
	"time"
	"encoding/hex"
)

type EthereumBlockchainConnector struct{}

var ethClient *ethclient.Client

// Connect to the Ethereum node
func (bc EthereumBlockchainConnector) Connect(url string) error {
	var err error
	ethClient, err = ethclient.Dial(url)
	return err
}

// Return single block
func (bc EthereumBlockchainConnector) GetBlockByNumber(blockNr *big.Int) (*types.Block, error) {
	block, err := ethClient.BlockByNumber(context.Background(), blockNr)
	return block, err
}

// Returns all ERC20 "transfer" transactions from startBlock to toBlock
func (bc EthereumBlockchainConnector) GetERC20Transfers(address string, startBlock *big.Int, toBlock *big.Int, transactions chan<- typex.Transaction) {
	log.Println("Retrieve ERC20 transfers for", address, "starting from", startBlock, "to", toBlock)
	addresses := []common.Address{common.HexToAddress(address)} // Proxeus' contract
	transferHashByte := crypto.Keccak256([]byte("Transfer(address,address,uint256)"))
	transferHash := common.BytesToHash(transferHashByte)
	// Do it blocks by blocks as we can't retrieve too many Log at once
	startBlocks, toBlocks := getBlockChunks(startBlock, toBlock, 500)
	var totalFound = 0
	for i, block := range startBlocks {
		log.Println("Retrieving logs for blocks", block, "up to", toBlocks[i], ". Transactions found until now: ", totalFound)
		logs, err := ethClient.FilterLogs(context.Background(), ethereum.FilterQuery{
			FromBlock: block,
			ToBlock:   toBlocks[i],
			Addresses: addresses,
			Topics:    [][]common.Hash{{transferHash}},
		})
		if err != nil {
			log.Fatal(err)
		}
		for _, Log := range logs {
			block, err := bc.GetBlockByNumber(big.NewInt(int64(Log.BlockNumber))) // Retrieve block's struct again as the event doesn't contain the time
			if err != nil {
				log.Fatal("Error retrieving block", Log.BlockNumber, err)
			}
			stringValue := hex.EncodeToString(Log.Data)
			value, success := new(big.Int).SetString(stringValue, 16)
			log.Println("Number", value);
			if success {
				log.Println("Adding transaction to channel", block.Number())
				transactions <- typex.Transaction{
					Address:     Log.Address.String(),
					Hash:        Log.TxHash.String(),
					Topic:       "transfer",
					BlockNumber: Log.BlockNumber,
					Value:       value,
					Time:        time.Unix(block.Header().Time.Int64(), 0),
				}
				totalFound++

			}
		}
	}
}

/*
Splits blocks in chunks of `size`. For example, given startBlock 10, toBlock 30 and size 3
should return an array with  [10, 13], [14, 17], [18, 21],..
 */
func getBlockChunks(startBlock *big.Int, toBlock *big.Int, size int) ([]*big.Int, []*big.Int) {
	if startBlock == nil || toBlock == nil {
		log.Fatal("startBlock and toBlock parameters can't be nil")
	}
	var startBlocks []*big.Int
	var toBlocks []*big.Int
	counter := 0
	for i := startBlock; i.CmpAbs(toBlock) < 0; i.Add(i, big.NewInt(int64(size))) {
		startBlockTemp := new(big.Int).Set(i) // we want it by value not reference
		toBlockTemp := new(big.Int).Set(i)
		toBlockTemp.Add(toBlockTemp, big.NewInt(int64(size)))
		if toBlockTemp.Cmp(toBlock) > 0 {
			toBlockTemp.Set(toBlock)
		}
		if counter == 0 {
			startBlocks = append(startBlocks, startBlockTemp)
		} else {
			startBlockTemp.Add(startBlockTemp, big.NewInt(1))
			startBlocks = append(startBlocks, startBlockTemp)
		}
		toBlocks = append(toBlocks, toBlockTemp)
		counter++
	}
	return startBlocks, toBlocks
}

// Ethereum client doesn't support "debug_traceTransaction"
func (bc EthereumBlockchainConnector) DebugTransaction(hash string) (string, error) {
	json := `{
		"id": 1, 
		"method": "debug_traceTransaction", 
		"params": [
			"` + hash + `", 
			{
				"disableStack": true, 
				"disableMemory": true, 
				"disableStorage": true
			}
		]
	}`
	jsonByte := []byte(json)
	req, _ := http.NewRequest("POST", ethereumNodeAddress, bytes.NewBuffer(jsonByte))
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	log.Println("Request info: " + resp.Status + " " + string(body))
	return string(body), err
}
