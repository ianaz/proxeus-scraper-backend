package service

import (
	"github.com/ethereum/go-ethereum/core/types"
	"math/big"
	"proxeus.com/scraper/typex"
)

// Global blockchain-connector interface. Can be implemented to connect to different blockchains

type BlockchainConnector interface {
	Connect(string) error
	GetBlockByNumber(*big.Int) (*types.Block, error)
	DebugTransaction(string) (string, error)
	GetERC20Transfers(contract string, startBlock *big.Int, toBlock *big.Int, transactions chan<- typex.Transaction)
}